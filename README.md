# BKIE REST Web Services Ejemplo

El ejemplo consiste en una aplicación que expone el recurso *Usuario* como un servicio web REST usando el framework 
[Flask](http://flask.pocoo.org/), una base de datos NoSQL [MongoDB](http://www.mongodb.com/), accedida a través de la 
librería [Flask-MongoEngine](http://flask-mongoengine.readthedocs.org/en/latest/), y en la cual se van a guardar toda 
la data necesaria para el funcionamiento de la aplicación. 

La aplicación consiste principalmente de dos módulos:
 
 * **Recursos**: Carpeta *resources*. Es esta se encuentra la implementación de las fachadas REST hacia los recursos 
 expuestos por la aplicación. Para el caso del recurso *Usuario*, se definen tres operaciones: Consultar todos los 
 usuarios (get_all), Consultar usuario (get) y crear usuario nuevo (post). El formato, tanto del cuerpo de la petición 
 a los servicios, como el de la respuesta de los mismos. es en JSON.
 
 * **Modelos**: Carpeta *models*. En esta se define e implementa la estrucutura de datos de los recursos a exponer, 
 así como relación de los paramétros definido en el modelo con la estructura del documento a guardar en la base de datos 
 NoSQL.

## Instrucciones de instalación y despliegue (Ubuntu)

Para que la aplicación funcione es necesario instalar los siguientes componentes: Python 2.7, pip + virtualenv. 
MongoDB Server 2.6 , Apache Server 2.4 con mod_wsgi 2.3.5. Posteriormente, se crea un ambiente virtual para la 
aplicación y se instalan las librerías necesarias.
 
### Python 2.7

Instalar los siguientes paquetes:

    sudo apt-get install build-essential
    sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev

Descargar Python 2.7 (En la carpeta destinada a esto):

    cd <Carpeta_Python>
    wget http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz

Extraer el contenido del paquete descargado y abrir la carpeta destino:

    tar -xvf Python-2.7.5.tgz
    cd Python-2.7.5

Instalar:

    ./configure
    make
    sudo make install

### pip + virtualenv

Estos componentes son necesarios para la descarga de paquetes y librerías en Python, y para crear ambientes 
virtualizados (Virtual Environment) para correr aplicaciones

    sudo apt-get install python-pip python-dev build-essential 
    sudo pip install --upgrade pip 
    sudo pip install --upgrade virtualenv

### MongoDB Server 2.6

Instalar:

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
    echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org=2.6.1 mongodb-org-server=2.6.1 mongodb-org-shell=2.6.1 mongodb-org-mongos=2.6.1 mongodb-org-tools=2.6.1

Iniciar el servidor:

    sudo service mongod start

Para más información acerca del proceso de instalación visitar la documentación de MongoDB 
[aquí](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/).

### Apache HTTP Server 2.4

El siguiente comando instalará la última versión de Apache HTTP Server:

    sudo apt-get install apache2

Iniciar el servidor:

    sudo service apache2 start

Instalar y habilitar mod_wsgi:

    sudo apt-get install libapache2-mod-wsgi
    sudo a2enmod wsgi

### Ambiente Virtual y librerías

Descargar los archivos en este repositorio en la ruta */var/www/*.

Crear un ambiente vurtual para la aplicación y activarlo:

    cd /var/www/bkierest/
    sudo pip install virtualenv
    sudo virtualenv venv
    source venv/bin/activate 


Instalar las librerías necesarias para que corrar la aplicación:

    sudo pip install flask
    sudo pip install mongoengine
    sudo pip install flask-mongoengine

### Configuración de la aplicación

En el archivo *__init.py__* en la carpeta */var/www/bkierest/bkierest/* se definen una variables configurables 
relacionadas con la conexión a servidor MongoDB. Cada uno de estas se describen a continuación:

 * **BD_NAME**: Nombre de la Base de Datos en MongoDB a la que se quiere acceder.
 * **MONGODB_SERVER_HOST**: Hostname del servidor MongoDB al que se desea conectar
 * **MONGODB_SERVER_PORT**: Puerto por el que está escuchando el servidor MongoDB al que se desea conectar
  
La parametrización por defecto en el código es para conectarse a un servidor MongoDB corriendo local con la 
configuración inicial por defecto (*localhost:27017*)

### Prueba

Para verificar que toda la instalación se realizó correctamente, correr la aplicación localmente:

    sudo python run.py

Se debería mostrar en consola el mensaje *“Running on http://localhost:5000/”*. En este momento se desplegaría el
recurso *Usuario*, el cual contiene las siguientes tres operaciones:

 * **GET /users**: Consulta todos los usuarios creados en el sistema. De primer momento no habría ninguno, por lo que 
 el servicio devolvería una lista vacía en formato JSON ([])
 * **GET /user/{userId}**: Consulta el usuario con identificador igual a *userid*. De primer momento no habría ningun
 usuario en el sistema, por lo que el servicio arrojaría un código HTTP 404 (NOT FOUND)
 * **POST /user**: Crea un usuario nuevo en el sistema. La información de este se pasa en formato JSON, por lo que hay 
 que especificarlo en la cabecera del mensaje (headers) a través del parámetro *Content-Type*: 
 Content-Type: application/son. La especificación de la información a enviar a través del cuerpo del mensaje es la 
 siguiente:
    
        {
            "device": "string",
            "first_name": "string",
            "last_name": "string",
            "email": "string",
            "password": "string"
        }
        
Para desactivar el ambienter virtual, ejecutar el siguiente comando:

    deactivate

### Configuración y despliegue

Configurar en la aplicacion en Apache, moviendo el archivo de configuración de la carpeta *config/* a 
*/etc/apache2/sites-available/*, y habilitarla:

    sudo cp /var/www/bkierest/config/bkierest.conf /etc/apache2/sites-available/
    sudo a2ensite bkierest
    sudo service apache2 restart

Para más información acerca de este proceso, visitar el siguiente tutorial 
[aquí](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-flask-application-on-an-ubuntu-vps).

Después de tener el servidor Apache iniciado, se puede probar la aplicación usando como dirección raíz 
*http://localhost/bkie-rest/*
