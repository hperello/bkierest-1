from flask import Flask
from flask.ext.mongoengine import MongoEngine

class Config(object):
    DEBUG = True
    MONGODB_HOST = ('mongodb://localhost:27017/'
                    'test?replicaSet=bkie-rs')
    MONGODB_DB = True

''' Apliccation initialization '''
app = Flask(__name__)
app.config["SECRET_KEY"] = "KeepThisS3cr3t"
app.config.from_object(Config)
db = MongoEngine(app)

from resources import user

