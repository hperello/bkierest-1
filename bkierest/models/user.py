from bkierest import db

class User(db.Document):
    device = db.StringField(max_length=2, required=True)
    first_name = db.StringField(max_length=255, required=True)
    last_name = db.StringField(max_length=255, required=True)
    email = db.StringField(required=True)
    password = db.StringField(required=True)