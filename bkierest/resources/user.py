from bkierest import app
import json
from bson import json_util
from bkierest.models.user import User
from flask import request

@app.route('/users', methods=['GET'])
def get_all():
    users = User.objects.all()
    return users.to_json()
 
@app.route('/user/<user_email>', methods=['GET'])
def get(user_email):
    user = User.objects.get_or_404(email=user_email)
    return user.to_json()

@app.route('/user', methods=['POST'])
def post():
    content = request.get_json()
    new_user = User()
    new_user = new_user.from_json(json.dumps(content, default=json_util.default))
    new_user.save()
    return new_user.to_json()

@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    return 'OK'
